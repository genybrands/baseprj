package geny.it.courseproject.functional;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import geny.it.courseproject.R;
import geny.it.courseproject.activity.HomeActivity;
import geny.it.courseproject.activity.HomeActivity_;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by luigi on 26/05/16.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class HomeActivityTest {

    private String mStringToBetyped;

    @Rule
    public ActivityTestRule<HomeActivity_> mActivityRule = new ActivityTestRule<>(
            HomeActivity_.class);

    @Before
    public void initValidString() {
        // Specify a valid string.
        mStringToBetyped = "Una gran bella etichetta";
    }

    @Test
    public void changeText_sameActivity() {

        onView(ViewMatchers.withId(R.id.tv_act_home_label))
                .check(matches(withText(mStringToBetyped)));
    }
}
