package geny.it.courseproject.integration;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import junit.framework.Assert;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.util.List;

import geny.it.courseproject.activity.TestActivity_;
import geny.it.courseproject.network.GithubService;
import geny.it.courseproject.network.model.Repo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.mockito.Mockito.when;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class GithubServiceTest {

    @Rule
    public ActivityTestRule<TestActivity_> testActivity_activityTestRule = new ActivityTestRule<TestActivity_>(TestActivity_.class);

    @Test
    public void testRepoGithub() {
        GithubService service = testActivity_activityTestRule.getActivity().getService();

        Call<List<Repo>> call = service.listRepos("acrive82");
        call.enqueue(new Callback<List<Repo>>() {
            @Override
            public void onResponse(Call<List<Repo>> call, Response<List<Repo>> response) {
                Assert.assertTrue(response.isSuccessful());
                Assert.assertTrue(response.body().size() > 0);
            }

            @Override
            public void onFailure(Call<List<Repo>> call, Throwable t) {
                Assert.fail();
            }
        });


    }


}
