package geny.it.courseproject.integration;

import android.content.SharedPreferences;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.SmallTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.util.Calendar;

import geny.it.courseproject.activity.TestActivity_;
import geny.it.courseproject.util.SharedPreferencesHelper;
import geny.it.courseproject.util.SharedPreferencesModel;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

/**
 * Test esempio con mock  (with Mockito)
 * <p/>
 * Questa classe di test utilizza la lib. mockito per generare dei mock. In questo caso l'oggetto mockato è la classe SharedPreferences e il suo editor (classe nested di SharedPreferences per
 * aggiornate le SharedPreferences.
 * <p/>
 * Nel codice c'è un model che rappresenta le informazioni che vogliamo salavare nelle SharedPreferences: <i>SharedPreferencesModel</i>.
 * Nel @Before ci occuperemo di popolare questi dati di esempio.
 * <p/>
 * Mockando l'oggetto SharedPreferences (con mockito) si mette a disposizione l'interfaccia <i>OnGoingStubbing</i>
 * Con questa interfaccia è possibile specificare delle operazioni da eseguire, nel momento in cui viene chiamato uno specifico metodo dell'oggetto mockato.
 * <p/>
 * Nel nostro caso:
 * quando effettuiamo un .getString(nomeParametroDellaSharedPreferences) possiamo specificare noi il risultato da mostrare. Esempio dalla riga 89 in poi...
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class SharedPreferencesTest {

    private SharedPreferencesModel mSharedPreferencesModel;
    private SharedPreferencesHelper mMockSharedPreferencesHelper;

    private static final String TEST_EMAIL = "luigi.candita@email.com";
    private static final String TEST_NAME = "Luis";
    private static final Calendar TEST_DATE_OF_BIRTH = Calendar.getInstance();

    static {
        TEST_DATE_OF_BIRTH.set(1982, 5, 5);
    }

    @Rule
    public ActivityTestRule<TestActivity_> testActivity_activityTestRule = new ActivityTestRule<TestActivity_>(TestActivity_.class);

    @Before
    public void initMocks() {
        mSharedPreferencesModel = new SharedPreferencesModel(TEST_NAME, TEST_EMAIL);
        mMockSharedPreferencesHelper = createMockSharedPreferences();
    }

    @Test
    public void saveSharedPreferences() {
        boolean success = mMockSharedPreferencesHelper.savePersonalInfo(mSharedPreferencesModel);

        assertThat("Test inserimento SharedPreferences", success, is(true));

        SharedPreferencesModel savedSharedPreferenceEntry = mMockSharedPreferencesHelper.getPersonalInfo();

        assertThat("SharedPreference - Test nome", mSharedPreferencesModel.getName(), is(equalTo(savedSharedPreferenceEntry.getName())));
        assertThat("SharedPreference - Test mail ", mSharedPreferencesModel.getEmail(), is(equalTo(savedSharedPreferenceEntry.getEmail())));

    }

    private SharedPreferencesHelper createMockSharedPreferences() {

        SharedPreferences mMockSharedPreferences = Mockito.mock(SharedPreferences.class);
        SharedPreferences.Editor mMockEditor = Mockito.mock(SharedPreferences.Editor.class);

        when(mMockSharedPreferences.getString(eq(SharedPreferencesHelper.KEY_NAME), anyString())).thenReturn(mSharedPreferencesModel.getName());
        when(mMockSharedPreferences.getString(eq(SharedPreferencesHelper.KEY_EMAIL), anyString())).thenReturn(mSharedPreferencesModel.getEmail());
        when(mMockEditor.commit()).thenReturn(true);
        when(mMockSharedPreferences.edit()).thenReturn(mMockEditor);


        return new SharedPreferencesHelper(mMockSharedPreferences);
    }
}
