package geny.it.courseproject.network;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;

import java.util.List;

import geny.it.courseproject.network.model.Repo;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by luigi on 06/06/16.
 */

@EBean
public class GithubService {

    private IGithubService service;

    @AfterInject
    public void doSomethingAfterInjection() {
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(IGithubService.class);
    }

    public Call<List<Repo>> listRepos(String username) {
        return service.listRepos(username);
    }

}
