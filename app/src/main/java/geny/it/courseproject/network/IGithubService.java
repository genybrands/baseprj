package geny.it.courseproject.network;

import java.util.List;

import geny.it.courseproject.network.model.Repo;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by luigi on 25/05/16.
 */
public interface IGithubService {
    @GET("users/{user}/repos")
    Call<List<Repo>> listRepos(@Path("user") String user);
}
