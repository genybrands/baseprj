package geny.it.courseproject.network.model;

/**
 * Created by luigi on 25/05/16.
 */
public class Repo {
    public String description;
    public String name;

    public Repo(String description, String name) {
        this.description = description;
        this.name = name;
    }

    public Repo() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
