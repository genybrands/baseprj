package geny.it.courseproject.fragment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import geny.it.courseproject.R;

/**
 * Created by luigi on 25/05/16.
 */
public class GithubViewHolder extends RecyclerView.ViewHolder {

    TextView tv_name;
    TextView tv_description;
    ImageView iv_test;

    public GithubViewHolder(View itemView) {
        super(itemView);

        tv_name = (TextView) itemView.findViewById(R.id.tv_name);
        tv_description = (TextView) itemView.findViewById(R.id.tv_description);

    }
}
