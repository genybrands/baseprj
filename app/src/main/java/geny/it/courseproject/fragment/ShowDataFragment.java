package geny.it.courseproject.fragment;


import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import geny.it.courseproject.Constants;
import geny.it.courseproject.R;
import geny.it.courseproject.db.FeedDataSource;
import geny.it.courseproject.db.FeedEntry.FeedEntry;

@EFragment(R.layout.fragment_show_data)
public class ShowDataFragment extends Fragment {


    @ViewById(R.id.recycler_view)
    RecyclerView recycler_view;

    private RecyclerViewFeedAdapter mAdapter;

//    public ShowDataFragment() {
//        // Required empty public constructor
//    }



    @AfterViews
    public void initView() {
        FeedDataSource dao = new FeedDataSource(getContext());
        dao.open();
        List<FeedEntry> data = dao.getAllFeedEntries();
        mAdapter = new RecyclerViewFeedAdapter(getContext(),data);
        recycler_view.setAdapter(mAdapter);
        dao.close();
        Log.i(Constants.LOG_TAG,data.toString());
    }

//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        View view =  inflater.inflate(R.layout.fragment_show_data, container, false);
//
//        recycler_view = (RecyclerView) view.findViewById(R.id.recycler_view);
//        FeedDataSource dao = new FeedDataSource(getContext());
//        dao.open();
//        List<FeedEntry> data = dao.getAllFeedEntries();
//        mAdapter = new RecyclerViewFeedAdapter(getContext(),data);
//        recycler_view.setAdapter(mAdapter);
//        dao.close();
//        Log.i(Constants.LOG_TAG,data.toString());
//
//        return view;
//
//    }

}
