package geny.it.courseproject.fragment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import geny.it.courseproject.R;
import geny.it.courseproject.db.FeedEntry.FeedEntry;

/**
 * Created by luigi on 25/05/16.
 */
public class RecyclerViewFeedAdapter extends RecyclerView.Adapter<FeedViewHolder> {

    private List<FeedEntry> mList;
    private Context context;

    public RecyclerViewFeedAdapter(Context context, List<FeedEntry> mList) {
        this.context = context;
        this.mList = mList;
    }

    @Override
    public FeedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.show_data_item, parent, false);
        FeedViewHolder holder = new FeedViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(FeedViewHolder holder, int position) {

        final FeedEntry feedEntry = mList.get(position);
        holder.tv_id.setText("" + feedEntry.getId());
        holder.tv_title.setText(feedEntry.getTitle());
        holder.tv_content.setText(feedEntry.getContent());

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
