package geny.it.courseproject.fragment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import geny.it.courseproject.R;
import geny.it.courseproject.network.model.Repo;

/**
 * Created by luigi on 25/05/16.
 */
public class RecyclerViewGithubAdapter extends RecyclerView.Adapter<GithubViewHolder> {

    private List<Repo> mList;
    private Context context;

    public RecyclerViewGithubAdapter(Context context, List<Repo> mList) {
        this.context = context;
        this.mList = mList;
    }

    @Override
    public GithubViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.show_repo_item, parent, false);
        GithubViewHolder holder = new GithubViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(GithubViewHolder holder, int position) {

        final Repo repo = mList.get(position);

        holder.tv_name.setText(repo.getName());
        holder.tv_description.setText(repo.getDescription());

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
