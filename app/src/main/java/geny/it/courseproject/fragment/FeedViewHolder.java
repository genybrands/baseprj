package geny.it.courseproject.fragment;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import geny.it.courseproject.R;

/**
 * Created by luigi on 25/05/16.
 */
public class FeedViewHolder extends RecyclerView.ViewHolder {

    TextView tv_id;
    TextView tv_title;
    TextView tv_content;

    public FeedViewHolder(View itemView) {
        super(itemView);

        tv_id = (TextView) itemView.findViewById(R.id.tv_id);
        tv_title = (TextView) itemView.findViewById(R.id.tv_title);
        tv_content = (TextView) itemView.findViewById(R.id.tv_content);

    }
}
