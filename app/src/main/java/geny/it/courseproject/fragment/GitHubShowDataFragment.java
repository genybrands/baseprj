package geny.it.courseproject.fragment;


import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import geny.it.courseproject.R;
import geny.it.courseproject.network.GithubService;
import geny.it.courseproject.network.IGithubService;
import geny.it.courseproject.network.model.Repo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@EFragment(R.layout.fragment_github_how_data)
public class GitHubShowDataFragment extends Fragment implements Callback<List<Repo>> {


    @ViewById(R.id.recycler_view)
    RecyclerView recycler_view;
    @ViewById
    TextInputEditText input_text;

    @ViewById
    ImageButton btn_search;

    @Bean
    GithubService service;

    private RecyclerViewGithubAdapter mAdapter;
    private List<Repo> mList = new ArrayList<>();


    @AfterViews
    public void initView() {


    }


    @Click(R.id.btn_search)
    public void clickSearch() {

        String user = input_text.getText().toString();
        remoteCall(user);
    }

    private void remoteCall(String user) {
        Call<List<Repo>> call = service.listRepos(user);
        //asynchronous call
        call.enqueue(this);
    }


    @Override
    public void onResponse(Call<List<Repo>> call, Response<List<Repo>> response) {
        List<Repo> repoList = response.body();
        mAdapter = new RecyclerViewGithubAdapter(getContext(), repoList);
        recycler_view.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onFailure(Call<List<Repo>> call, Throwable t) {
        Toast.makeText(getContext(), t.getLocalizedMessage() + " WTF", Toast.LENGTH_SHORT).show();

    }
}
