package geny.it.courseproject.util;

public class SharedPreferencesModel {

    private final String mName;
    private final String mEmail;

    public SharedPreferencesModel(String name,  String email) {
        mName = name;
        mEmail = email;
    }

    public String getName() {
        return mName;
    }


    public String getEmail() {
        return mEmail;
    }
}