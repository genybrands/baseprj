package geny.it.courseproject.util;

import android.content.SharedPreferences;

import java.util.Calendar;

/**
 * Created by luigi on 06/06/16.
 */
public class SharedPreferencesHelper {

    public static final String KEY_NAME = "sp_key_name";
    public static final String KEY_EMAIL = "sp_key_email";

    private final SharedPreferences mSharedPreferences;

    public SharedPreferencesHelper(SharedPreferences sharedPreferences) {
        mSharedPreferences = sharedPreferences;
    }

    public boolean savePersonalInfo(SharedPreferencesModel model) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString(KEY_NAME, model.getName());
        editor.putString(KEY_EMAIL, model.getEmail());

        return editor.commit();
    }

    public SharedPreferencesModel getPersonalInfo() {

        String name = mSharedPreferences.getString(KEY_NAME, "");
        String email = mSharedPreferences.getString(KEY_EMAIL, "");

        return new SharedPreferencesModel(name, email);
    }
}
