package geny.it.courseproject.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;

import geny.it.courseproject.R;
import geny.it.courseproject.network.GithubService;

@EActivity(R.layout.test_act)
public class TestActivity extends AppCompatActivity {

    @Bean
    GithubService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public GithubService getService() {
        return service;
    }
}


