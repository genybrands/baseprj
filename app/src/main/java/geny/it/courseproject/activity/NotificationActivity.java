package geny.it.courseproject.activity;

import android.app.NotificationManager;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import geny.it.courseproject.R;

@EActivity(R.layout.activity_notification)
public class NotificationActivity extends AppCompatActivity {

    @ViewById
    Button btn_notification;

    @AfterViews
    public void initView() {
        btn_notification = (Button) findViewById(R.id.btn_notification);

        btn_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(NotificationActivity.this)
                                .setSmallIcon(R.drawable.ic_accessibility_black_24dp)
                                .setContentTitle("Hello Uorld")
                                .setContentText("Content Text della mia notifica");

                int mNotificationId = 001;
                NotificationManager mNotifyMgr =
                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                mNotifyMgr.notify(mNotificationId, mBuilder.build());

            }
        });
    }

   
}
