package geny.it.courseproject.activity;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import geny.it.courseproject.R;
import geny.it.courseproject.fragment.ComplexFragment;
import geny.it.courseproject.fragment.ComplexFragment_;
import geny.it.courseproject.fragment.GitHubShowDataFragment;
import geny.it.courseproject.fragment.GitHubShowDataFragment_;
import geny.it.courseproject.fragment.MyBaseFragment;
import geny.it.courseproject.fragment.MyBaseFragment_;
import geny.it.courseproject.fragment.ShowDataFragment;
import geny.it.courseproject.fragment.ShowDataFragment_;

@EActivity(R.layout.activity_home)
public class HomeActivity extends AppCompatActivity implements ComplexFragment.OnFragmentInteractionListener {

    @ViewById
    Button btn_act_home_first,
            btn_act_home_second,
            btn_github,
            btn_act_home_db,
            btn_act_home_open_fragment,
            btn_act_home_open_complex_fragment;
    @ViewById
    TextView tv_act_home_label;

    @ViewById(R.id.ll_act_home)
    LinearLayout rootLayout;

    @ViewById(R.id.fragment_container)
    RelativeLayout fragment_container;


    @AfterViews
    public void initView() {
        for (final Button button : getAllButtonView(rootLayout)) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(), "Bottone " + button.getText().toString(), Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_home);
//
//        rootLayout = (LinearLayout) findViewById(R.id.ll_act_home);
//
//        btn_act_home_first = (Button) findViewById(R.id.btn_act_home_first);
//        btn_act_home_second = (Button) findViewById(R.id.btn_act_home_second);
//        btn_act_home_db = (Button) findViewById(R.id.btn_act_home_db);
//        btn_github = (Button) findViewById(R.id.btn_github);
//        btn_act_home_open_fragment = (Button) findViewById(R.id.btn_act_home_open_fragment);
//        btn_act_home_open_complex_fragment = (Button) findViewById(R.id.btn_act_home_open_complex_fragment);
//        tv_act_home_label = (TextView) findViewById(R.id.tv_act_home_label);
//        fragment_container = (RelativeLayout) findViewById(R.id.fragment_container);
//
//
//        for (final Button button : getAllButtonView(rootLayout)) {
//            button.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(), "Bottone " + button.getText().toString(), Toast.LENGTH_SHORT).show();
//                }
//            });
//        }
//
//        btn_act_home_open_fragment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                MyBaseFragment fragment = new MyBaseFragment();
//                getSupportFragmentManager()
//                        .beginTransaction()
//                        .replace(R.id.fragment_container, fragment)
//                        .addToBackStack(null)
//                        .commit();
//            }
//        });
//
//        btn_act_home_open_complex_fragment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ComplexFragment fragment = ComplexFragment.newInstance("param1", "param2");
//                getSupportFragmentManager()
//                        .beginTransaction()
//                        .replace(R.id.fragment_container, fragment)
//                        .addToBackStack(null)
//                        .commit();
//            }
//        });
//
//        btn_act_home_db.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ShowDataFragment showDataFragment = new ShowDataFragment();
//                getSupportFragmentManager()
//                        .beginTransaction()
//                        .replace(R.id.fragment_container, showDataFragment)
//                        .commit();
//
//            }
//        });
//        btn_github.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                GitHubShowDataFragment fragment = new GitHubShowDataFragment();
//                getSupportFragmentManager()
//                        .beginTransaction()
//                        .replace(R.id.fragment_container, fragment)
//                        .commit();
//            }
//        });
//    }

    private List<Button> getAllButtonView(ViewGroup root) {
        List<Button> views = new ArrayList<>();
        for (int i = 0; i < root.getChildCount(); i++) {
            View v = root.getChildAt(i);
            if (v instanceof Button) {
                views.add((Button) v);
            } else if (v instanceof ViewGroup) {
                views.addAll(getAllButtonView((ViewGroup) v));
            }
        }
        return views;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        Log.d("TAG", "---------------------------------" + uri.toString());
    }

    @Click(R.id.btn_act_home_open_fragment)
    public void clickBtnActHomeOpenFragment() {
        MyBaseFragment fragment = MyBaseFragment_.builder().build();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Click(R.id.btn_act_home_open_complex_fragment)
    public void clickBtnActHomeOpenComplexFragment() {
        ComplexFragment fragment = ComplexFragment_.builder().mParam1("Param1").mParam2("Param2").build();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Click(R.id.btn_act_home_db)
    public void clickBtnActHomeDB() {
        ShowDataFragment showDataFragment = ShowDataFragment_.builder().build();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, showDataFragment)
                .commit();
    }

    @Click(R.id.btn_github)
    public void clickBtnGithub() {
        GitHubShowDataFragment fragment = GitHubShowDataFragment_.builder().build();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();
    }
}
