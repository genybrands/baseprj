package geny.it.courseproject.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import geny.it.courseproject.Constants;
import geny.it.courseproject.R;
import geny.it.courseproject.db.FeedEntry.FeedEntry;
import geny.it.courseproject.db.FeedEntry.FeedReaderDbHelper;

@EActivity(R.layout.activity_splash)
public class SplashActivity extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH = 2500;
    private Handler mHandler = new Handler();

    @AfterViews
    public void initView() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                MainActivity_.intent(SplashActivity.this).start();
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);

        bootstrapDB();
    }

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_splash);
//
//
//
//    }

//    public int ticktock = 0;
//
//    public void onCreate(Bundle b) {
//        super.onCreate(b);
//        setContentView(new RenderableView(this) {
//            @Override
//            public void view() {
//
//                relativeLayout(() -> {
//                    textView(() -> {
//                        size(WRAP, WRAP);
//                        text(android.R.string.cut);
//                    });
//
//                    button(() -> {
//                        text("Next Activity");
//                        size(MATCH, WRAP);
//                        centerVertical();
//                        alignParentLeft();
//                        alignParentStart();
//                        onClick(v -> {
//                            Toast.makeText(getContext(), "Hello ciao", Toast.LENGTH_SHORT).show();
//                        });
//                    });
//                });
//
//            }
//        });
//    }

    private void bootstrapDB() {
        FeedReaderDbHelper mDbHelper = new FeedReaderDbHelper(this);
        SQLiteDatabase sqLiteDatabase = mDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(FeedEntry.COLUMN_NAME_TITLE, "Titolo 1");
        values.put(FeedEntry.COLUMN_NAME_CONTENT, "Contenuto 1");

        ContentValues values2 = new ContentValues();
        values2.put(FeedEntry.COLUMN_NAME_TITLE, "Titolo 2");
        values2.put(FeedEntry.COLUMN_NAME_CONTENT, "Contenuto 2");

        long id1 = sqLiteDatabase.insert(FeedEntry.TABLE_NAME, null, values);
        long id2 = sqLiteDatabase.insert(FeedEntry.TABLE_NAME, null, values2);
        Log.i(Constants.LOG_TAG, "Inserito " + id1);
        Log.i(Constants.LOG_TAG, "Inserito " + id2);


    }

}
