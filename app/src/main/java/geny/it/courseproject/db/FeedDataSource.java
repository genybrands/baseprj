package geny.it.courseproject.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import geny.it.courseproject.db.FeedEntry.FeedEntry;
import geny.it.courseproject.db.FeedEntry.FeedReaderDbHelper;
import geny.it.courseproject.fragment.FeedViewHolder;

/**
 * Created by luigi on 25/05/16.
 */
public class FeedDataSource {

    private SQLiteDatabase database;
    private FeedReaderDbHelper dbHelper;
    private String[] allColumns = {FeedEntry.COLUMN_NAME_ENTRY_ID,
            FeedEntry.COLUMN_NAME_TITLE, FeedEntry.COLUMN_NAME_CONTENT};

    public FeedDataSource(Context context) {
        dbHelper = new FeedReaderDbHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public FeedEntry addFeedEntry(String title, String content) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FeedEntry.COLUMN_NAME_CONTENT, content);
        contentValues.put(FeedEntry.COLUMN_NAME_TITLE, title);

        long id = database.insert(FeedEntry.TABLE_NAME, null, contentValues);

        Cursor cursor = database.query(FeedEntry.TABLE_NAME, allColumns, FeedEntry.COLUMN_NAME_ENTRY_ID + " = " + id, null, null, null, null);
        cursor.moveToFirst();
        FeedEntry feedEntry = getFeedEntryByCursor(cursor);
        return feedEntry;
    }

    public List<FeedEntry> getAllFeedEntries() {

        List<FeedEntry> result = new ArrayList<FeedEntry>();

        Cursor cursor = database.query(FeedEntry.TABLE_NAME,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            FeedEntry feed = getFeedEntryByCursor(cursor);
            result.add(feed);
            cursor.moveToNext();
        }

        cursor.close();
        return result;

    }

    public void deleteFeedEntry(FeedEntry feed) {
        long id = feed.getId();
        System.out.println("Feed deleted with id: " + id);
        database.delete(FeedEntry.TABLE_NAME, FeedEntry.COLUMN_NAME_ENTRY_ID
                + " = " + id, null);
    }

    private FeedEntry getFeedEntryByCursor(Cursor cursor) {
        FeedEntry feedEntry = new FeedEntry();
        feedEntry.setId(cursor.getLong(0));
        feedEntry.setTitle(cursor.getString(1));
        feedEntry.setContent(cursor.getString(2));
        return feedEntry;
    }
}
