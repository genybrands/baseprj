package geny.it.courseproject.db.FeedEntry;

/**
 * Created by luigi on 25/05/16.
 */
public class FeedEntry {
    public static String TABLE_NAME = "FEED";
    public static String _ID = "ID";
    public static String COLUMN_NAME_ENTRY_ID = "ID";
    public static String COLUMN_NAME_TITLE = "TITLE";
    public static String COLUMN_NAME_CONTENT = "CONTENT";

    String title;
    String content;
    long id;

    public FeedEntry() {
    }

    public FeedEntry(String content, long id, String title) {
        this.content = content;
        this.id = id;
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
